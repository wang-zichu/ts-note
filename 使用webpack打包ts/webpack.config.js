const path = require('path')
const HTMLWebPackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
module.exports = {
    entry:'./src/index.ts',
    // 指定打包文件所在的目录
    output:{
        // 指定打包文件的目录
        path:path.resolve(__dirname,'dist'),
        // 打包后的文件的文件
        filename:'bundle.js'
    },
    // 指定webpack打包时要使用的模块
    module:{
        // 指定要加载的规则
        rules:[
            {
                // 指定规则生效的文件
                test:/\.ts$/,
                use:[{  
                        // 指定加载器
                        loader:'babel-loader',
                        options:{
                            //预定义环境
                            presets:[
                                [   
                                    // 指定环境插件
                                    '@babel/preset-env',
                                    // 配置信息
                                    {   
                                        //要兼容的浏览器
                                        targets:{
                                            "chrome":"88"
                                        },
                                        // 指定corejs的版本
                                        "corejs":"3",
                                        // 使用corejs的方式"usage"表示按需加载
                                        "useBuiltIns":"usage"
                                    }
                                ]
                            ]
                        }
                    },'ts-loader'],
                exclude:/node-modules/
            }
        ]
    },
    // 自动执行修改
    plugins:[
        new CleanWebpackPlugin(),
        new HTMLWebPackPlugin({
            template:'./src/index.html'
        }),
    ],
    mode: 'development',
    // 可以引入的文件格式
    resolve:{
        extensions:['.js','.ts']
    }
}