(()=>{
    class Person{
        // TS可以在属性前添加修饰符
        /*
            public 修饰的属性可以任意的修改和访问
                包括子类
            private 私有属性，私有属性只能在类的内部访问和修改
                通过在类中添加方法使私有属性不可以被外部访问包括子类
            protected 受保护的属性，只能在当前类和其子类中访问和修改
        */
        private _name:string
        private _age:number
        constructor(name:string,age:number){
            this._name = name
            this._age = age
        }
        // getter方法用来读取属性
        // setter方法用于设置属性
        //      他们被称为属性的存取器
        // 定义一个方法，用来获取name属性
        // getName(){
        //     return this.name
        // }
        // getAge(){
        //     return this.age
        // }
        // // 定义一个方法用来设置属性
        // setName(name:string){
        //     this.name = name
        // }
        // setAge(age:number){
        //     age>=0 ? this.age = age : console.log('不能小于0')
        // }
        // 在TS中设置getter方法时
        get name(){
            return this._name
        }
        get age(){
            return this._age
        }
        // 在TS中设置setter方法
        set name(name:string){
            this._name = name
        }
        set age(age:number){
            age>=0 ? this._age = age : console.log('值不合法')
        }
    }
    
    const per = new Person('王子初',22)

    console.log(per)

    // per.setName('刘明')
    // per.getName()
    per.name = '王子'
    console.log(per)
    // 语法糖写起来更方便
    class C {
        constructor(public name:string,public age:number){
        }
    }
    const c = new C('xxx',12)
    console.log(c)
})()