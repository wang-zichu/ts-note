class Dog{
    name : String
    age : number
    //constructor构造函数，在对象创建的时候执行
    constructor(name:string , age:number){
        // 在构造函数中，this指向就是创建的那个对象
        this.name = name
        this.age = age
    }

    bark(){
        console.log(this,'汪汪汪')
    }

}

const dog = new Dog( '旺财' , 2 )
const dog2 = new Dog( '小白' , 4 )

console.log(dog , dog2)

dog.bark()
dog2.bark()