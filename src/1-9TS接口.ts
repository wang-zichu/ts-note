(()=>{
    // 描述一个对象的类型
    type myType = {
        name:string,
        age:number
    }
    /*
        接口用来定义一个类结构
        用来定义一个类中一个包含哪些属性和方法
        同时接口可以当成类型声明去使用
        接口可以重复定义，重复定义的话会把属性增加在一起
    */
   interface myInterface{
       name:string
       age:number
   }
   interface myInterface{
       gender:string
   }
   const obj:myInterface = {
       name:'sss',
       age:111,
       gender:'男'
   }
   /*
     接口可以限制类的结构
        接口的所有属性都不能有实际的值
        接口只定义对象的结构，而不考虑实际值
        在接口中所有的方法都是抽象方法
   */
  interface myInter{
      name:string
      sayHello():void
  }
  /*
    定义类的时候可以使类去实现一个接口，实现接口就是满足我们接口的要求
  */
 class MyClass implements myInterface{
     name
     age
     gender
     constructor(name:string,age:number,gender:string){
        this.name = name
        this.age = age 
        this.gender = gender
     }
 }
 console.log(new MyClass('王子初',18,'男'))
})()