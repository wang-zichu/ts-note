(()=>{
    // 在我们定义函数或者时类，如果遇到类型不明确就可以使用泛型
    function fn<T>(a: T): T{
        return a
    }
    // 直接调用有泛型的函数
    fn(10)//不指定泛型，TS可以自动对类型进行判断
    fn<string>('hello')//指定泛型
    // 泛型可以指定多个
    function fn2<T , K>( a: T, b: K){
        console.log(b)
        return a
    }
    fn2<number,string>(123,'wzc')
    // 可以对泛型进行限制
    interface Inter{
        length:number
    }
    // 泛型T必须时Inter的一个子类
    function fn3<T extends Inter>(a: T): number{
        return a.length
    }
    fn3('123')
    class MyClass<T>{
        constructor(public name:T){}
    }
    const mc = new MyClass<string>('wzc')
    console.log(mc)
})()