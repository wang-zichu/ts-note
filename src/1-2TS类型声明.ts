//声明一个a，指定一个类型是number，后面使用a必须是number类型
// let a:number
// a = 10
// a = true //指定的类型是number不能赋值其他类型
// let b:string
// b = 'hello'
// let c: boolean = true//平常语法
// 如果我们变量的声明和赋值时同时进行的，TS会自动进行类型检测
// let d = true
// d =123 //我们在赋值的时候会自动给你声明
// js的函数时不考虑参数的个数和类型的
// function add(a,b){
//     return a+b
// }
// add(123,456)//579
// add(132,'456')//'123456'
// 当我们使用ts定义时可以对参数进行类型确定，我们还可以在参数括号后面定义函数返回值的类型
// function add(a: number,b: number): number{
    // return a+b
// }
// add(123,456)//579
// add(123,'456')//报错，参数类型不正确
// add(123,456,789)//报错，参数的数量必须相同


/*

*/