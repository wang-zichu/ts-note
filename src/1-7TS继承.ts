(()=>{
    class Animal{
        name
        age
        constructor(name:string,age:number){
            this.name = name
            this.age = age 
        }
        sayHello(){
            console.log('动物在叫')
        }
    }
    /*
        animal被称为父类，dog被称为子类
        使用继承后子类会拥有父类所有的方法和属性
        子类可以覆盖掉父类的方法，这种形式我们成为方法的重写
    */
    class Dog extends Animal{
        sayHello(){
            console.log('汪汪汪')
        }
        run(){
            console.log(this.name+'在跑')
        }
    }
    class Cat extends Animal{
        color
        // 如果我们在子类中写了构造函数，子类的构造函数会覆盖父类的构造函数，在子类的构造函数中我们必须对父类的构造函数调用
        constructor(name:string,age:number,color:string){
            super(name,age)
            this.color = color
        }
    }
    const dog = new Dog('旺财',5)
    const cat = new Cat('咪咪',2,'白色')
    console.log(dog) 
    console.log(cat)
    dog.sayHello()
    dog.run()
    cat.sayHello()
})()