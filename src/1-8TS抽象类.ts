(()=>{
    // 抽象类，抽象类和其他的类差不多，但是不能创建抽象类的实例，
    //  抽象类一般都是用于继承的
    abstract class Animal{
        name
        age
        constructor(name:string,age:number){
            this.name = name
            this.age = age 
        }
        // 抽象方法
        // 抽象方法使用abstract作为开头，没有方法体，
        // 抽象方法只能定义在抽象类中，而且子类必须对抽象方法进行重写
        abstract sayHello():void
    }
    class Cat extends Animal{
        color
        constructor(name:string,age:number,color:string){
            super(name,age)
            this.color = color
        }
        sayHello(){
            console.log('喵喵喵')
        }
    }
    const cat = new Cat('咪咪',2,'白色')
    console.log(cat)
    cat.sayHello()
})()