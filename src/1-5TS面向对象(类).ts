/*
    一、类(class)
        定义类
            
*/
class Parson { 
    /*
        直接定义的属性是实例属性，需要通过对象的实例来访问
            const per = new Person()
            per.name
        使用static开头的属性是静态属性(类属性)，可以直接通过类去访问
            Person.age
        使用readonly是一个只读的属性，如果像创建一个只读的静态属性static要在前面
    */
    // 定义实例属性
    name = '王子初'
    // 在属性面前使用static关键字可以定义类属性(静态属性)
    static age = 18
    // 只读属性
    readonly perstc = '男'

    // 方法
    sayHello(){
        console.log('hello 大家好')
    }
    // 静态方法,可以直接使用类来调用
    static sayHellon(){
        console.log('hello 大家好我是一个静态方法')
    }
}

const per = new Parson();

console.log(per)

console.log(per.name)

per.sayHello()

Parson.sayHellon()
