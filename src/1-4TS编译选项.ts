/*
    tsc 文件名 -w                            表示进入监视编译模式，但是只能监视一个文件，在开发中不方便使用
    tsc -w                                   进入监视模式会监视文件夹下所有的文件，但是必须有tsconfig.json,配置文件
*/