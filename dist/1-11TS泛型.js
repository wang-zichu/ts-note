"use strict";
(() => {
    function fn(a) {
        return a;
    }
    fn(10);
    fn('hello');
    function fn2(a, b) {
        console.log(b);
        return a;
    }
    fn2(123, 'wzc');
    function fn3(a) {
        return a.length;
    }
    fn3('123');
    class MyClass {
        constructor(name) {
            this.name = name;
        }
    }
    const mc = new MyClass('wzc');
    console.log(mc);
})();
