"use strict";
(() => {
    class Person {
        constructor(name, age) {
            this._name = name;
            this._age = age;
        }
        get name() {
            return this._name;
        }
        get age() {
            return this._age;
        }
        set name(name) {
            this._name = name;
        }
        set age(age) {
            age >= 0 ? this._age = age : console.log('值不合法');
        }
    }
    const per = new Person('王子初', 22);
    console.log(per);
    per.name = '王子';
    console.log(per);
    class C {
        constructor(name, age) {
            this.name = name;
            this.age = age;
        }
    }
    const c = new C('xxx', 12);
    console.log(c);
})();
