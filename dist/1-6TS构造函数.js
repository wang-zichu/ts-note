"use strict";
class Dog {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
    bark() {
        console.log(this, '汪汪汪');
    }
}
const dog = new Dog('旺财', 2);
const dog2 = new Dog('小白', 4);
console.log(dog, dog2);
dog.bark();
dog2.bark();
