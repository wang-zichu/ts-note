"use strict";
(() => {
    class Animal {
        constructor(name, age) {
            this.name = name;
            this.age = age;
        }
    }
    class Cat extends Animal {
        constructor(name, age, color) {
            super(name, age);
            this.color = color;
        }
        sayHello() {
            console.log('喵喵喵');
        }
    }
    const cat = new Cat('咪咪', 2, '白色');
    console.log(cat);
    cat.sayHello();
})();
