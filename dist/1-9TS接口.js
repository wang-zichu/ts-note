"use strict";
(() => {
    const obj = {
        name: 'sss',
        age: 111,
        gender: '男'
    };
    class MyClass {
        constructor(name, age, gender) {
            this.name = name;
            this.age = age;
            this.gender = gender;
        }
    }
    console.log(new MyClass('王子初', 18, '男'));
})();
