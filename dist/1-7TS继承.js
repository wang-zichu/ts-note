"use strict";
(() => {
    class Animal {
        constructor(name, age) {
            this.name = name;
            this.age = age;
        }
        sayHello() {
            console.log('动物在叫');
        }
    }
    class Dog extends Animal {
        sayHello() {
            console.log('汪汪汪');
        }
        run() {
            console.log(this.name + '在跑');
        }
    }
    class Cat extends Animal {
        constructor(name, age, color) {
            super(name, age);
            this.color = color;
        }
    }
    const dog = new Dog('旺财', 5);
    const cat = new Cat('咪咪', 2, '白色');
    console.log(dog);
    console.log(cat);
    dog.sayHello();
    dog.run();
    cat.sayHello();
})();
