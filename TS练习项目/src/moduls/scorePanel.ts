class SorePanel{
    score = 0
    level = 1
    scoreEle = document.getElementById('score')!
    levelEle = document.getElementById('level')!
    constructor(public maxLevel = 10,public upSocre = 10){}
    // 设置一个加分的方法
    addScore(){
        this.scoreEle.innerHTML = ++this.score+''
        // 判断分数是多少
        if(this.score%this.upSocre == 0){
            this.upLevel()
        }
    }
    // 升级的方法
    upLevel(){
        if(this.level<this.maxLevel)
            this.levelEle.innerHTML = ++this.level+''
    }
    // 
}
export default SorePanel