class Snake {
    head: HTMLElement = document.querySelector('#sanke>div')!
    element: HTMLElement = document.getElementById('sanke')!
    bodies: HTMLCollection = this.element.getElementsByTagName('div')
    // 获取蛇头的坐标
    get X() {
        return this.head.offsetLeft
    }
    get Y() {
        return this.head.offsetTop
    }
    set X(value: number) {
        if (this.X === value)
            return
        if (value < 0 || value > 290) {
            throw new Error('蛇死了')
        }
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetLeft === value) {

            if (value > this.X) {
                value = this.X - 10
            } else {
                value = this.X + 10
            }
        }
        this.movBody()
        this.head.style.left = value + 'px'
        this.checkHeadBody()
    }
    set Y(value: number) {
        if (this.Y === value)
            return
        if (value < 0 || value > 290) {
            throw new Error('蛇死了')
        }
        if (this.bodies[1] && (this.bodies[1] as HTMLElement).offsetTop === value) {
            if (value > this.Y) {
                value = this.Y - 10
            } else {
                value = this.Y + 10
            }
        }
        this.movBody()
        this.head.style.top = value + 'px'
        this.checkHeadBody()
    }
    addBody() {
        this.element.insertAdjacentHTML("beforeend", "<div></div>")
    }
    movBody() {
        for (let i = this.bodies.length - 1; i > 0; i--) {
            let X = (this.bodies[i - 1] as HTMLElement).offsetLeft
            let Y = (this.bodies[i - 1] as HTMLElement).offsetTop;
            (this.bodies[i] as HTMLElement).style.left = X + 'px';
            (this.bodies[i] as HTMLElement).style.top = Y + 'px'
        }
    }
    checkHeadBody() {
        if (this.bodies.length > 1) {
            for(let i = 1 ; i < this.bodies.length ; i++){
                let bd = this.bodies[i] as HTMLElement
                if(this.X == bd.offsetLeft && this.Y == bd.offsetTop){
                    throw new Error('自杀式死亡')
                }
            }
        }
    }
}
export default Snake