class Food{
    element:HTMLElement  = document.getElementById('food')!
    bodies: HTMLCollection =  document.getElementById('sanke')!.getElementsByTagName('div')
    // 获取食物X轴坐标的方法
    get X(){
        return this.element.offsetLeft
    }
    // 获取食物Y轴坐标的方法
    get Y(){
        return this.element.offsetTop
    }
    // 修改食物位置的方法
    change(){
        interface typeData{
            X:number,
            Y:number
        }
        let coorArr : typeData[] = []
        let bodieArr : typeData[] = []
        for(let i = 0 ; i < 30 ; i++){
            for(let n = 0 ; n < 30 ; n++){
                let m= {
                    X:i,
                    Y:n
                }
                coorArr = [...coorArr,m]
            }
        }
        for(let i = 0 ; i < this.bodies.length ; i++){
            let m = {
                X:(this.bodies[i] as HTMLElement).offsetLeft/10,
                Y:(this.bodies[i] as HTMLElement).offsetTop/10
            }
            bodieArr = [...bodieArr,m]
        }
        bodieArr.forEach(bodie=>{
            coorArr.splice(coorArr.findIndex(coor=>coor.X==bodie.X&&coor.Y==bodie.Y),1)
        })
        let num = Math.floor(Math.random()*coorArr.length)
        let top = coorArr[num].X
        let left = coorArr[num].Y
        this.element.style.left = top*10+'px'
        this.element.style.top = left*10+'px'
    }
}
export default Food