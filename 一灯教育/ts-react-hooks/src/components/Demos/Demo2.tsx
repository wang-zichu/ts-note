import React, { ReactNode, useReducer } from 'react'

const initialState = {count:0}

type ACTIONTYPE = 
    {type:'increment';payload:number} |
    {type:'decrement';payload:string} 
function reducer(state:typeof initialState,action:ACTIONTYPE){
    switch (action.type){
        case "increment":
            return{
                count:state.count + action.payload
            }
        case "decrement":
            return{
                count:state.count - parseInt(action.payload)
            }
        default:
            throw new Error("测试")
    }
}
const Demo2 = ():ReactNode =>{
    const [state,dispatch] = useReducer(reducer,initialState)
    return(
        <>
            <div>Demo2{state}</div>
            <button onClick={()=>{dispatch({type:'increment',payload:30})}}></button>
        </> 
    )
}
export default Demo2