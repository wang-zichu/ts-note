import React, { ReactNode, useEffect } from 'react'

const Demo3 = (props:{timerMs:number}):ReactNode =>{
    const {timerMs} = props
    useEffect(() =>{
        setTimeout(() =>{
            console.log('wzc')
        },timerMs)
    },[timerMs])
    return(
        <>
            <div>Demo3</div>
        </>
    )
}
export default Demo3