/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {ReactNode, useRef, useEffect} from 'react';

const Demo4 = ():ReactNode =>{
    // 断言！不是null
    const ref1 = useRef<HTMLDivElement>(null!)
    const ref2 = useRef<HTMLButtonElement>(null)
    const ref3 = useRef<HTMLDivElement | null>(null)
    useEffect(() =>{
        console.log(ref1.current)
    })
    return (
        <>
            <div ref={ref1}>Dome4</div>
            <button type="button" ref={ref2}/> 
        </>
    )
}

export default Demo4