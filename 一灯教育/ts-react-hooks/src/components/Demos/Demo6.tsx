import React, {VFC} from 'react';
type GreetProps = {age?:number} & typeof defaultProps
const defaultProps = {age:25}
const Demo6:VFC<GreetProps> = (props:GreetProps) => {
    const {age} = props
    return <h1>{age}</h1>
}

export default Demo6
