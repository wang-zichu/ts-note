import React,{ forwardRef, ReactNode , Ref, useCallback, useImperativeHandle, useRef} from "react";
type ListProps = {
    innerRef:Ref<{to():void}>
}
const Demo5 = (props:ListProps):ReactNode=>{
    useImperativeHandle(props.innerRef,()=>({
        to(){
            console.log(123)
        }
    }))
    return(
        <></>
    )
}

export default Demo5
// const TestRef = forwardRef((props,ref)=>{
//     const divRef = useRef<HTMLDivElement>(null!)
//     useImperativeHandle(ref,()=>({
//         open(){
//             console.log('open')
//         }
//     }))
//     return <div ref={divRef}></div>
// })
// function App(){
//     const divRef = useRef<HTMLDivElement>()
//     divRef.current.open()
//     return(
//         <>
//             <div>wzc</div>
//             <TestRef ref={divRef}/>
//         </>
        
//     )
// }