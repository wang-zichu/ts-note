import React, { ReactNode } from 'react'
interface IProps{
    name: string;
}
const defaultProps = {
    age:25
}
const GreetComponent = ({name,age}:IProps & typeof defaultProps):JSX.Element=>(
    <div>{`${name}--${age}`}</div>
)
GreetComponent.defaultProps = defaultProps
type ComponentProps<T> = T extends 
    |React.ComponentType<infer P>
    |React.Component<infer P>
    ?JSX.LibraryManagedAttributes<T,P>:never
// type xx = ComponentProps<typeof GreetComponent>
const TestComponent = (props:ComponentProps<typeof GreetComponent>):JSX.Element=>{
    const {name} = props
    return <h1>{name}</h1>
}
const App = ():ReactNode => <TestComponent name={"bbb"} />
// eslint-disable-next-line import/no-anonymous-default-export
export default {
    GreetComponent,
    TestComponent,
    App
}