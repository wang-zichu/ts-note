import React , { useState , useReducer } from 'react'
type IUser = { 
    username:string
}
const Demo1 = () => {
    // const [val,toggle] = useState(false)
    const [user,setUser] = useState<IUser | null>(null)
    setUser({username:'wzc'})
    console.log(user?.username ?? 'wzc')
    return (
        <div>Demo1</div>
    )
}
export default Demo1