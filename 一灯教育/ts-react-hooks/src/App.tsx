import React from 'react';
import Demos from './components/Demos';

function App() {
  return (
    <div className="App">
      App...
      <Demos/>
    </div>
  );
}

export default App;
