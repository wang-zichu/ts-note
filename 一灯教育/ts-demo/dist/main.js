/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/demos20.ts":
/*!************************!*\
  !*** ./src/demos20.ts ***!
  \************************/
/***/ (() => {

eval("function _classCallCheck(instance, Constructor) {\n    if (!(instance instanceof Constructor)) {\n        throw new TypeError(\"Cannot call a class as a function\");\n    }\n}\nvar User = function User(name) {\n    \"use strict\";\n    _classCallCheck(this, User);\n    this.name = name;\n    console.log(this.name);\n};\nvar constr = {\n    type: User\n};\n// constr.type == new (...args:string) => User\nvar userInstance = new constr.type('wzc');\nconsole.log(userInstance);\n\n\n//# sourceURL=webpack://ts-demo/./src/demos20.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/demos20.ts"]();
/******/ 	
/******/ })()
;