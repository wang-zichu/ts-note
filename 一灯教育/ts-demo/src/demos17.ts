// apply使用时的常用错误
interface Cat{
    name: string
}
const bobTheCat:Cat ={
    name: "Bob"
}
function printsCatName(this: Cat){
    console.log(this.name)
}
// 执行不出来
printsCatName.apply(bobTheCat)