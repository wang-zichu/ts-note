const DEV = Symbol('dev')
const PROD = Symbol('prod')

const obj = {
    [DEV]:30
}
function showWarning(msg:string,mode: typeof DEV | typeof PROD) {
    console.log(mode.description,msg)
}
showWarning('出错了',PROD)