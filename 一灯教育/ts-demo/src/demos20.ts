// 构造参数类型
class User{
    constructor(public name:string) {
        console.log(this.name);
    }
}
interface IConstruct<T extends new (...args: any) => any>{
    // 核心在这里
    // ioc装载到容器中需要进行校验
    type:new (...args:ConstructorParameters<T>) => InstanceType<T>
}
type UserConstruct = IConstruct<typeof User>
const constr:UserConstruct = {
    type:User
}
// constr.type == new (...args:string) => User
const  userInstance = new constr.type('wzc')
console.log(userInstance)