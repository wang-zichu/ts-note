// function getLength(str:string|number):number{
//     return str. length
// }
// 强制类型断言(<string>str)
function getLength(str:string|number):number{
    // if((str as string).length)
    if((<string>str).length){
        return (<string>str).length;
    }else{
        return str.toString().length
    }
}
type typeName = string;
type NameResolver = () => string
type NameOrResolver = typeName | NameResolver
function getName(n:NameResolver):typeName{
    if(typeof n === "string"){
        return n
    }else{
        return n()
    }
}

// interface A{
//     msg:string
// }
// function helper(options:A):A{
//     return options
// }
// const xxA:A = {}
declare module 'koa-swig'{
    interface window{
        a:number
    }
    interface window{
        b:number
    }
}

