import 'reflect-metadata';
function inject(serviceIdentifier:string){
    return function(target: Object,targetKey:string,index:number){
        Reflect.defineMetadata(serviceIdentifier,"wzc666",target)
    }
}
class IndexController {
    public indexService: string;
    constructor(@inject('xxx')indexService: string){
        this.indexService = indexService
    }
}
//IOC注入
const indexController = new IndexController('wzc')
console.log(indexController.indexService)