let isDone:boolean = true
let num: number = 0
let hexLiteral: number = 0xf00d
let myName:string = "王子初"

function alerName(name:string):string{
    return "测试"+name
}

alerName("王子初")

let unusable:void = undefined

function getString(something:string|number):string{
    return something.toString()
}

getString("文本132")

//一些固定的参数集合
enum Days{
    Sun = 7,
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat
}
console.log(Days['Sun'])

interface Person{
    readonly id: number,
    name?:string,
    age:number,
    [proName:string]:any
}
const wzc:Person = {
    id:20,
    age:20
}
wzc.age = 60
// type