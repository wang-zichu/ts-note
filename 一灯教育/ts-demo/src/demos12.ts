const data = {
    a:3, 
    hello: "hello"
}
// function get(o:object,name:string){
//     return o[name]
// }
// console.log(get('cc'))
function get<T extends object,K extends keyof T>(o:T,name:K){
    return o[name]
}
console.log(get(data,'hello'))
export default data
