let myNum:(x:number,y:number)=>number=function(
    x:number,
    y:number
):number{
    return x+y
}
// 函数的默认参数
function bulidName(firstName:string,lastName:string = 'Cat'){
    return firstName+lastName
}
// 剩余参数
function push(array:any[],...items:any[]){
    items.forEach(itme => {
        array.push(itme)
    });
}
let a = [4]
push(a,1,2,3)
console.log(a)
// 可选参数
function bulidName2(firstName:string,lastName?:string){
    if (lastName) {
        return firstName+lastName
    } else {
        return lastName
    }
}
const tom = bulidName2("王")
const tom2 = bulidName2("王","子初")
// 函数
// {}
// ()=>{};
// function