type CoreIconName = 'user' | 'customer'
const opts:CoreIconName = 'customer'
// 解决代码提示问题
type LiteralUnion<T extends U,U=string> = T | ( U & {} )
interface GreetSettings{
    greeting:string;
    duration:number;
    color?:false;
}
type Color = LiteralUnion<'red' | 'black' | keyof GreetSettings>
const c:Color = "greeting"