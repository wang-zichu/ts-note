// 检测参数不为null
type NonNulllableType = string | number | null | undefined
function showType(args:NonNullable<NonNulllableType>){
    console.log(args)
}
showType('wzc')
showType(666)
// showType(null)
// 将参数全转化为string
type StringMap<T> = { 
    [P in keyof T]:string
}
function showType2(args:StringMap<{id:number;name:string}>){
    console.log(args)
}
