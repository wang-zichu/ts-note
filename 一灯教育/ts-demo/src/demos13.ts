interface User{
    id: number;
    age: number;
    name: string;
}
// 把所有必须的字段变成可选的字段
type PartialUser = Partial<User>;
// 把所有的可选字段变成必须字段
type PullDownUser = Required<PartialUser>;
// 选择一些必选字段，生成一个新的接口
type PickUser = Pick<User,'id'|'age'>;
// 排除一些字段，生成一个新的接口
type OmitUser = Omit<User,'id'|'age'>;
// 排除掉两个类型的交集
type A = Exclude<'x'|'a','x'>
// 案例
type Selet = 'id'|'age'
type PartialSelet = Partial<Pick<User, Selet>>
type OmitUserSelet = Omit<User,Selet>
// 合并
type Fianl = PartialSelet & OmitUser
const s : Fianl = {
    name:'wzc',
    age:30
}
// 让部分可选，再让部分不可选
type SeletPartial<T,V extends keyof T> = Partial<Pick<T, V>>&Omit<T,V>
// 提取
interface FirstType{
    id:number;
    firstName:string;
    lastName:string;
}
interface SecondType{
    id:number;
    address:string;
    city:string;
}
// 交集和补集
type ExtractType = Extract<keyof FirstType,keyof SecondType>
type ExcludeType = Exclude<keyof FirstType,keyof SecondType>
export default s