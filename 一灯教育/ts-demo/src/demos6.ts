// 泛型
interface LengthWise{
    length: number;
}
function idemnity<T extends LengthWise>(arg:T):T{
    console.log(arg.length);
    return arg
}
const result = idemnity<string>('wscz')
console.log(result)


// 类型和实体类
class GenericNumber<T>{
    zeroVale:T | undefined
    add:((x: T, y: T) => T) | undefined
}
const myGenericNumber = new GenericNumber<number>()
myGenericNumber.zeroVale = 0
myGenericNumber.add = function(x: number, y: number){
    return x + y
}
console.log(myGenericNumber.add(30,50))


// 重载
function getData<T>(value:T):T{
    return value
}
getData<number>(123)
getData<string>('王子初')

// 泛型接口
interface ConfigFn{
    <T>(value:T):void
}
const getData2:ConfigFn = function<T>(value:T):void{
    console.log(value)
}
getData2<string>("123")

// 动态的泛型
interface Bookmark{
    msg:string
}
interface Book {
    msg:string,
    msg1:string
}
class BookmarkServer<T extends Bookmark>{
    items:T[] = []
}
class BookmarkServer2<T extends Bookmark = Bookmark>{
    items:T[] = []
}
const s = new BookmarkServer2<Book>()

//readonly仅仅可以修饰元组和数组
// let err1: readonly Set<number>
// let err2: readonly Array<number>

let ok1: readonly number[]
let ok2: readonly [number , string]