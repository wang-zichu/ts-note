/**
 *1.interface和type区别
 *  相同点
 *      1-1都可以描述一个对象或者函数
 *      1-2都允许进行扩展
 *  不同点
 *      1-1type可以去声明基本类型别名、联合类型、元组
 *      1-2typeof获取实例对象
 *      1-3interface可以被合并
 */
/**
 *1.有关于后台的接口 原因去使用interface
 *2.第三方开发的SDK 比如VUE
 *3.前端的库 wzc.min.js wzc.d.ts
 *4.正常开发的任务type直接用更方便一些
 */
// interface IPriceData{
//     id: number
//     m: string
// }
// type IPriceDataArray = IPriceData[]

// function getIPriceData(){
//     return new Promise<IPriceDataArray>((resolve, reject) => {
//         fetch('url')
//         .then((response) => {
//             return response.json();
//         }).then((myJson)=>{
//             const data:IPriceDataArray = []
//             resolve(data)
//         })
//     })
// }

// getIPriceData().then(data => {
//     console.log(data[0].id)
// })
// node js 用的多一些
// interface ClockConstructor{
//     new (hour: number, minute: number, second: number):ClockInterface
// }
// interface ClockInterface{
//     tick():void;
// }
// class DigitalClock implements ClockInterface{
//     public tick(): void {
//         console.log("beep beep");
//     }
// }
// class AnalogClock implements ClockInterface{
//     public tick(): void {
//         console.log("ding ding");
//     }
// }

// function createClock(ctor:ClockConstructor,hour:number,minute: number,second:number):ClockInterface{
//     return new ctor(hour,minute,second)
// }

// const D = createClock(DigitalClock,12,17,50)
// const A = createClock(AnalogClock,11,15,30)
// D.tick()
// A.tick()