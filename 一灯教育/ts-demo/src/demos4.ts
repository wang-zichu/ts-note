/*
    1.抽象类可以具体的实现，也可以定义抽象的方法
    2.方法修饰符
        public      共有的  任何地方都可以使用
        private     私有的  不能在外部使用只能在内部使用，包括子类
        protected   保护的  不能在外部使用，但是可以在子类和内部使用
*/   
// 抽象类
abstract class Index{
    abstract makeSound():void
    public move():void{
        console.log("DY")
    }
}
// 既可以是一个类也可以是一个类型
class Dog extends Index{
    #region : string
    constructor(){
     super();
     this.#region = "王"
    }
    // private uname: string = "王子初"
    makeSound(){
        console.log('wzc')
    }
}

const dog: Index =  new Dog
dog.move()
dog.makeSound()
console.log((dog as any).region)

let index:Index[] = [dog]