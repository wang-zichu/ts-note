/**
 *
 *  一个方法
 * @param {string} message 提示信息
 * @param {(number|string)} code 错误码
 * @param {('dom1'|'dom2')} [type] 参数
 * @return {*}  {string} 返回错误信息
 * 
 * [还不明白？点击这里](http://www.baidu.com/)
 */
function getErrorMessage(
    message: string,
    code: number|string,
    type?: 'dom1'|'dom2'
): string {
    return (message||'网络繁忙请稍后'+code)
}