import { createApp } from 'vue'
import App from './App.vue'
import store from './stroe'

createApp(App).use(store).mount('#app')
