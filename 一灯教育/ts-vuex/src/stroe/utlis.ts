import {modules} from './modules'
//获取modules中所有的getters
type GetGetter<Module> = Module extends {getters: infer G} ? G : unknown
type GetGetters<Modules> = {
    [K in keyof Modules]:GetGetter<Modules[K]>
}
// 已经获取到所有的getters
type WZCGeters = GetGetters<typeof modules>

type AddPrefix<P , K> = `${P & string}/${K & string}`
// user/isLoading----user下面所有的key
type GetSpliceKey<P,Module> = AddPrefix<P,keyof Module>
type GetSpliceKeys<Modules> = {
    [K in keyof Modules]:GetSpliceKey<K,Modules[K]>
}[keyof Modules]
// 已经修改为我们想要的结构
type xx2 = GetSpliceKeys<WZCGeters>
// `${P & string}/${K & string}`当成key再把方法取出来
type GetFunc<T,A,B> = T[A & keyof T][B & keyof T[A & keyof T]]
type GetSpliceObj<T> = {
    [K in GetSpliceKeys<T>]:K extends `${infer A }/${infer B }`? GetFunc<T,A,B> : unknown
}
type ModulesGetters = GetSpliceObj<WZCGeters>
type Gtters = {
    [K in keyof ModulesGetters]:ReturnType<ModulesGetters[K]>
}
export {Gtters}