import {USER_STORE} from './constant'
import {ActionContext} from 'vuex'
import {userState} from './store'
export default {
    [USER_STORE.GET_ACTION_DATE]({commit}:ActionContext<userState,unknown>):void{
        console.log("action执行了")
        setTimeout(() =>{
            const payload = false
            commit(USER_STORE.GET_ACTION_DATE, payload)
        },2000)
    }
}