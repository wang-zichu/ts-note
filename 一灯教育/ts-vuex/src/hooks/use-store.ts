import { useStore } from 'vuex';
import type {State} from '../stroe/index'
import type {Gtters} from '../stroe/utlis'
interface IUseYdStore{
    state: State;
    getters: Gtters;
}
const useYdStore = ():IUseYdStore => {
    const store = useStore<State>()
    const { state , getters }:IUseYdStore = store
    return {
        state,
        getters,
    }
}
export {useYdStore} 
export default  useYdStore